# The Embroidermodder Project Website

This repository copies over the html and pdf manuals to the site [www.libembroidery.org](https://www.libembroidery.org).

This is maintained seperately from the other projects for the storage of large binaries:
like all the previous releases of Embroidermodder, libembroidery and EmbroideryMobile.

If this reaches the cap of storage offered by a github repository then we'll have to think
of something else since the version history of the binaries could quickly become important if
we have any regressions.
