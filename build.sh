#!/bin/bash

#sudo apt install doxygen
#
#./build.sh docs

git clone https://github.com/embroidermodder/embroidermodder
cp embroidermodder/embroidermodder*manual.pdf public
cp embroidermodder/embroidermodder*manual.pdf public/embroidermodder_current_manual.pdf
cp -R embroidermodder/assets/docs/html/* public

mv public ..
